tb	


format	
1	46	0	0	1	

sysformat	
1	0	0	1	/	.	,	

ClientFormatBlock	
1	1	1	1	/	.	,	

company4	
New Company Ltd	
	
	
	

					D	C	0		
0	1	1	1	0	0	0	0	0	0	0	0	0	0	0	
1	1	0	0	0	UK	ENG	
											

trgen	
1	1	1	1					0	0			0		0		0		0		0		

periods	
01/01/2007	31/12/2007	Y06	Year 2006	
01/01/2007	31/01/2007	0601	January 2007	
01/02/2007	28/02/2007	0602	February 2007	
01/03/2007	31/03/2007	0603	March 2007	
01/04/2007	30/04/2007	0604	April 2007	
01/05/2007	31/05/2007	0605	May 2007	
01/06/2007	30/06/2007	0606	June 2007	
01/07/2007	31/07/2007	0607	July 2007	
01/08/2007	31/08/2007	0608	August 2007	
01/09/2007	30/09/2007	0609	September 2007	
01/10/2007	31/10/2007	0610	October 2007	
01/11/2007	30/11/2007	0611	November 2007	
01/12/2007	31/12/2007	0612	December 2007	
01/01/2007	31/03/2007	06Q1	First Quarter	
01/04/2007	30/06/2007	06Q2	Second Quarter	
01/07/2007	30/09/2007	06Q3	Third Quarter	
01/10/2007	31/12/2007	06Q4	Fourth Quarter	


roundoff	
2	2	0	0	


LangNrVc	
ENG	English	billion	billion	billion	million	million	million	thousand	thousand	thousand	one hundred	two hundred	three hundred	four hundred	five hundred	six hundred	seven hundred	eight hundred	nine hundred	zero	and	and		1	0				0		
one		
two		
three		
four		
five		
six		
seven		
eight		
nine		
ten		
eleven		
twelve		
thirteen		
fourteen		
fifteen		
sixteen		
seventeen		
eighteen		
nineteen		
twenty		
twenty one		
twenty two		
twenty three		
twenty four		
twenty five		
twenty six		
twenty seven		
twenty eight		
twenty nine		
thirty		
thirty one		
thirty two		
thirty three		
thirty four		
thirty five		
thirty six		
thirty seven		
thirty eight		
thirty nine		
forty		
forty one		
forty two		
forty three		
forty four		
forty five		
forty six		
forty seven		
forty eight		
forty nine		
fifty 		
fifty one		
fifty two		
fifty three		
fifty four		
fifty five		
fifty six		
fifty seven		
fifty eight		
fifty nine		
sixty		
sixty one		
sixty two		
sixty three		
sixty four		
sixty five		
sixty six		
sixty seven		
sixty eight		
sixty nine		
seventy		
seventy one		
seventy two		
seventy three		
seventy four		
seventy five		
seventy six		
seventy seven		
seventy eight		
seventy nine		
eighty		
eighty one		
eighty two		
eighty three		
eighty four		
eighty five		
eighty six		
eighty seven		
eighty eight		
eighty nine		
ninety		
ninety one		
ninety two		
ninety three		
ninety four		
ninety five		
ninety six		
ninety seven		
ninety eight		
ninety nine		



country	
UK	United Kingdom	

	

WeekVc	
UK	UK	0	
1999	04/01/1999	52	
2000	03/01/2000	52	
2001	01/01/2001	52	
2002	07/01/2002	52	
2003	06/01/2003	52	
2004	05/01/2004	52	
2005	03/01/2005	52	
2006	02/01/2006	52	
2007	01/01/2007	52	
2008	07/01/2008	52	
				

vit	
1	1	1	0	0	0,00	0	


SpecDayVc	
BD	Boxing Day	

CD	Christmas Day	

EM	Easter Monday	

EMBH	Early May Bank Holiday	

GF	Good Friday	

NYD	New Year's Day	

SPBH	Spring Bank Holiday	

SUBH	Summer Bank Holiday	


BHollVc	
UK	Bank and Public Holidays in England and Wales		0	0	0	0	0	1	1	
3/1/2005	NYD	
25/3/2005	GF	
28/3/2005	EM	
2/5/2005	EMBH	
30/5/2005	SPBH	
29/8/2005	SUBH	
27/12/2005	CD	
26/12/2005	BD	
2/1/2006	NYD	
14/4/2006	GF	
17/4/2006	EM	
1/5/2006	EMBH	
29/5/2006	SPBH	
28/8/2006	SUBH	
25/12/2006	CD	
26/12/2006	BD	
1/1/2007	NYD	
6/4/2007	GF	
9/4/2007	EM	
7/5/2007	EMBH	
28/5/2007	SPBH	
27/8/2007	SUBH	
25/12/2007	CD	
26/12/2007	BD	
1/1/2008	NYD	
21/3/2008	GF	
24/3/2008	EM	
5/5/2008	EMBH	
26/5/2008	SPBH	
25/8/2008	SUBH	
25/12/2008	CD	
26/12/2008	BD	


DNMVc	
ENG	English	Monday	Tuesday	Wednesday	Thursday	Friday	Saturday	Sunday	January	February	March	April	May	June	July	August	September	October	November	December	


FormBlock	
0	1	

DefValDecimalsBlock	
2	2	
